import numpy as np
from states import States

class Grid(object):
    @classmethod
    def random(cls, size):
        return np.random.choice(list(States), size=size)

    @classmethod
    def empty(cls, size):
        return np.ones(size)*States.DEAD.value

    def __repr__(self):
        return 'Grid(\n  {}\n)'.format('\n  '.join([str(cells) for cells in self.state]))

    def __getitem__(self, indices):
        return self.state[indices]

    def __init__(self, dimensions, cell_size, is_random = False):
        self.dimensions = dimensions
        self.cell_size = cell_size
        self.state = Grid.random(dimensions) if is_random else Grid.empty(dimensions)

    def wipe(self):
        self.state[:] = Grid.empty(self.dimensions)[:]
        return self.state

    def randomize(self):
        self.state[:] = Grid.random(self.dimensions)[:]
        return self.state

    def reshape(self, next_dimensions):
        next_state = Grid.empty(next_dimensions)
        min_grid_rows = np.minimum(self.state.shape[0], next_state.shape[0])
        min_grid_cols = np.minimum(self.state.shape[1], next_state.shape[1])
        next_state[:min_grid_rows, :min_grid_cols] = self.state[:min_grid_rows, :min_grid_cols]
        self.state = next_state
        self.dimensions = next_dimensions
        return self.state

    def invert_cell_state(self, x, y):
        self.state[x, y] = list(States)[int(not self.state[x, y])]
        return self.state[x, y]

    def insert(self, Entity, x, y):
        entity = Entity(x, y)
        self.state[entity.location] = entity.arrangement

    def update(self):
        """
        1. Any live cell with two or three live neighbours survives.
        2, Any dead cell with three live neighbours becomes a live cell.
        3. All other live cells die in the next generation. Similarly, all other dead cells stay dead.
        """
        x_cells, y_cells = self.dimensions
        next_grid = self.state.copy()
        for x in np.arange(self.state.shape[0]):
            for y in np.arange(self.state.shape[1]):
                # right, bottom
                # left, top
                # top right, bottom right
                # top left, bottom left
                neighbour_sum = np.array((255 - self.state[(x + 1) % x_cells, y]) + (255 - self.state[x, (y + 1) % y_cells]) +
                                         (255 - self.state[(x - 1) % x_cells, y]) + (255 - self.state[x, (y - 1) % y_cells]) +
                                         (255 - self.state[(x + 1) % x_cells, (y + 1) % y_cells]) + (255 - self.state[(x + 1) % x_cells, (y - 1) % y_cells]) +
                                         (255 - self.state[(x - 1) % x_cells, (y + 1) % y_cells]) +  (255 - self.state[(x - 1) % x_cells, (y - 1) % y_cells])) / 255

                if self.state[x, y] == States.ALIVE.value and (neighbour_sum < 2 or neighbour_sum > 3):
                    # any live cell with fewer than two live neighbours dies, as if by underpopulation
                    # any live cell with more than three live neighbours dies, as if by overpopulation.
                    next_grid[x, y] = States.DEAD.value

                elif self.state[x, y] == States.DEAD.value and neighbour_sum == 3:
                    # any dead cell with three live neighbours becomes a live cell.
                    next_grid[x, y] = States.ALIVE.value

        self.state[:] = next_grid[:]

    def forest_fire_update(self):
        """
        1. If the tree is burning, the tree will stop burning if the sum is less than 3 (Not enough heat to ignite surrounding area) or greater than 6 (lack of oxygen)
        2. If the tree is burning, assign it the value of the sum - 2, as if to simulate flame intensity.
        3. If the tree is burning, a trees value cannot decrease unless assigned to 1 or 0, a tree value of 4 can only be reassigned to 0 or remain as 4.
        4. If the tree is not burning, a sum of exactly 4 will cause ignition and assign a value of 1 to the tree, sum of 5 will assign value of 2.
        5. A tree can burn for a set number of iterations. Each tree is assigned a strength value of 40-60 and can burn for until its strength reaches zero, where the strength degrades for every iteration that it burns.
        """
        return

