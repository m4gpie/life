import click
import pygame

from game import Game
from grid import Grid
from window import Window
from events import event_handler
from entities import Block, Glider, QueenBeeShuttle

@click.command()
@click.option('--cell-size', '-s', default=30, help='size of cells in pixels')
@click.option('--generations', '-g', default=10000, help='number of generations')
@click.option('--ticks', '-t', default=1, help='adjust speed of epoch')
@click.option('--random', '-r', is_flag=True, help='initialise the grid with a random distribution of living cells')
@click.option('--bg', default=(175, 175, 175), help='set the background colour')
@click.option('--autostart', default=False, help='start the game immediately')
def main(cell_size, generations, ticks, random, bg, autostart):
    pygame.init()
    clock = pygame.time.Clock()
    window = Window(cell_size)
    initial_dimensions = window.calculate_grid_dimensions(cell_size)
    game = Game(is_paused=not autostart)
    grid = Grid(initial_dimensions, cell_size, is_random=random)
    grid.insert(Block, 5, 8)
    grid.insert(Block, 25, 8)
    grid.insert(QueenBeeShuttle, 17, 5)
    window.render(grid)
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            event_handler(event, game, grid, window)

        window.render(grid)

        if not game.is_paused and game.epoch <= generations:
            game.epoch += 1
            grid.update()

        pygame.display.update()
        clock.tick(ticks)

if __name__ == '__main__':
    main()
