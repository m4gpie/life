import numpy as np
from states import States

class Entity():
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Block(Entity):
    arrangement = np.array([[States.ALIVE.value, States.ALIVE.value],
                            [States.ALIVE.value, States.ALIVE.value]]).T
    def __init__(self, x, y):
        Entity.__init__(self, x, y)
        self.location = tuple([slice(self.x, self.x + 2), slice(self.y, self.y + 2)])

class Glider(Entity):
    arrangement = np.array([[States.DEAD.value, States.DEAD.value, States.ALIVE.value],
                            [States.ALIVE.value, States.DEAD.value, States.ALIVE.value],
                            [States.DEAD.value, States.ALIVE.value, States.ALIVE.value]]).T

    def __init__(self, x, y):
        Entity.__init__(self, x, y)
        self.location = tuple([slice(self.x, self.x + 3), slice(self.y, self.y + 3)])

class QueenBeeShuttle(Entity):
    arrangement = np.array([[States.ALIVE.value, States.DEAD.value, States.DEAD.value, States.DEAD.value, States.DEAD.value],
                            [States.ALIVE.value, States.DEAD.value, States.ALIVE.value, States.DEAD.value, States.DEAD.value],
                            [States.DEAD.value, States.ALIVE.value, States.DEAD.value, States.ALIVE.value, States.DEAD.value],
                            [States.DEAD.value, States.ALIVE.value, States.DEAD.value, States.DEAD.value, States.ALIVE.value],
                            [States.DEAD.value, States.ALIVE.value, States.DEAD.value, States.ALIVE.value, States.DEAD.value],
                            [States.ALIVE.value, States.DEAD.value, States.ALIVE.value, States.DEAD.value, States.DEAD.value],
                            [States.ALIVE.value, States.DEAD.value, States.DEAD.value, States.DEAD.value, States.DEAD.value]]).T

    def __init__(self, x, y):
        Entity.__init__(self, x, y)
        self.location = tuple([slice(self.x, self.x + 5), slice(self.y, self.y + 7)])
