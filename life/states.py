from enum import Enum

class States(Enum):
    ALIVE = 0
    DEAD = 255
