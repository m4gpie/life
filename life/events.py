import numpy as np
from utils import is_changed_dimensions
import sys
import pygame
from pygame.locals import (QUIT,
                           VIDEORESIZE,
                           KEYDOWN,
                           K_SPACE,
                           K_ESCAPE,
                           K_RETURN,
                           K_BACKSPACE,
                           MOUSEBUTTONDOWN)

def is_quitting(event):
    return event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE)

def is_resizing(event):
    return event.type == VIDEORESIZE

def is_pausing(event):
    return event.type == KEYDOWN and event.key == K_SPACE

def is_wiping_grid(event):
    return event.type == KEYDOWN and event.key == K_BACKSPACE

def is_randomising_grid(event):
    return event.type == KEYDOWN and event.key == K_RETURN

def is_clicking(event):
    return event.type == MOUSEBUTTONDOWN

def event_handler(event, game, grid, window):
    if is_quitting(event):
        pygame.quit()
        sys.exit()
    elif is_resizing(event) and is_changed_dimensions(window.dimensions, (event.w, event.h)):
        window.resize(event.w, event.h)
        new_dimensions = window.calculate_grid_dimensions()
        if is_changed_dimensions(grid.dimensions, new_dimensions):
            grid.reshape(new_dimensions)
    elif is_pausing(event):
        game.is_paused = not game.is_paused
    elif is_wiping_grid(event):
        grid.wipe()
    elif is_randomising_grid(event):
        grid.randomize()
    elif is_clicking(event):
        mouse_x, mouse_y = event.pos
        cell_x, cell_y = int(np.floor(mouse_x / grid.cell_size)), int(np.floor(mouse_y / grid.cell_size))
        grid.invert_cell_state(cell_x, cell_y)
