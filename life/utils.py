def is_changed_dimensions(current_dimensions, next_dimensions):
    """check to see if (x,y) have changed in either direction"""
    current_x, current_y = current_dimensions
    next_x, next_y = next_dimensions
    return (next_x > current_x or next_x < current_x) or (next_y > current_y or next_y < current_y)
