import numpy as np
import pygame
from pygame.locals import HWSURFACE, DOUBLEBUF, RESIZABLE

class Window(object):
    def __init__(self, cell_size, dimensions=(500, 500), bg=(175, 175, 175)):
        self.screen = pygame.display.set_mode(dimensions, HWSURFACE | DOUBLEBUF | RESIZABLE)
        self.surface = pygame.display.get_surface()
        self.dimensions = self.surface.get_size()
        self.width = self.dimensions[0]
        self.height = self.dimensions[1]
        self.options_width = 200
        self.bg = bg

    def resize(self, width, height):
        self.width = width
        self.height = height
        self.dimensions = (self.width, self.height)
        return self.dimensions

    def calculate_grid_dimensions(self, cell_size):
        grid_width, grid_height = np.floor(self.width - self.options_width), self.height
        return int(np.floor(grid_width / cell_size)), int(np.floor(grid_height / cell_size))

    def render(self, grid):
        self.screen.fill(self.bg)
        for x in np.arange(grid.state.shape[0]):
            for y in np.arange(grid.state.shape[1]):
                rect = pygame.Rect(x * grid.cell_size, y * grid.cell_size, grid.cell_size - 1, grid.cell_size - 1)
                pygame.draw.rect(self.screen, (grid.state[x, y], grid.state[x, y], grid.state[x, y]), rect)

