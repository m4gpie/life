# Life

A python implementation of Conway's Game of Life (Cellular Automata) using pygame and click CLI

| ![png](./assets/life.png) |
|---------------------------|

## Setup

## Local Install

Python3 must be installed, see [this article for installation instructions](https://realpython.com/installing-python/#how-to-install-python-on-linux)

Once installed, a local installation is easy:

```bash
# choose an installation directory
LIFE_DIR=$HOME/.life
git clone git@git.kierangibb.co.uk:m4gpie/life.git $LIFE_DIR && cd $LIFE_DIR
# create a virtual python environment and activate it
python3 -m venv env
source env/bin/activate
# install to this environment
pip install --editable .
```

If you restart your computer, you'll need to reload the environment, simply run:

```bash
source $LIFE_DIR/env/bin/activate
```

## Uninstall

```bash
rm -rf $LIFE_DIR
```

## Usage

```bash
life --cell-size 10 /
     --generations 10000 / # number of generations the simulation will run before terminating
     --ticks 5 / # set the speed of the simulation
     --random # an optional flag to initialize the grid with a random distribution of living cells
```

### Controls

You can click on a cell to switch it's state (living / dead) to generate your own initial conditions.

Press the space bar to start and paused the game.

```
<ESC> Quit the game
<SPACE> Pause the game
<BACKSPACE> Clear the grid of living cells
```

## Ideas for Future

### Interface
- options bar for selecting parameters, initial conditions, etc
- $[x, y]$ display in bottom left corner
- display epoch options bar
- interface to insert entities at $[x, y]$
- interface to select a different set of rules for the game e.g. forest fire

### Forest Fire Simulation
See [new update rules](https://github.com/kevintpeng/CellularAutonomy)

### An Engine for Simulating Mycelial Growth?

Could we design a set of rules to simulate the spread, growth and mining of resources by a mycelial network within some media?

Supposing the following:

- a cell is a hypha, described by specific pixel colour
- a mycelia is the collection of hypha
- a hypha cannot occupy an existing square (e.g. itself, a resource, another mycelia)
- a hypha will not come within 1 square distance of an already existing square (not including the current cell in question) occupied by another hypha (proximity alert), but it can approach resources. this would mean it doesn't overlap itself, and keep it at a distance from rival mycelia
- in each epoch a hypha can:
  - keep growing in the same direction, i.e. expand into a neighbouring grid cell (on the horizontal, vertical, diagonal planes)
  - change direction / retreat / withdraw
  - divide and fork in two new directions
- a block is a resource, squares in the block can occupy several states of density (e.g. different shades of brown)
  - once in contact with the resource, a hypha can mine the resource instead of (as well as?) grow, decreasing the block's density each epoch until it reaches zero
  - if the mycelium is in contact with the resource, it can choose to change the direction / retract some hypha and redirect their growth towards the block to mine it faster
  - it can do this via a shortest path, so we can have a representation of the grid as an upper triangular graph matrix

#### Questions
- how does a mycelia decide which direction to grow? must have some degree of randomness (at least initially), but as it accumulates mass, it can perform shortest path computations to determine which direction to go? how do we systematically explore 2D space?
- what are the trigger conditions that would cause a fork?
- what are the conditions in which a mycelia will decay rather than exponentially grow? We need a balancing feedback loop.
- what happens if we introduce additional mycelia entities described by other colours?
- can we introduce rules of engagement between the mycelia? See Lynn Boddy's paper "Fungus Wars: Basidiomycete Battles in Wood Decay"

