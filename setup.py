from setuptools import setup, find_packages

setup(
    name='life',
    description='conways game of life using pygame',
    version=open('VERSION').read().strip(),
    author='magpie',
    author_email='kg@magmacollective.org',
    url='https://git.kierangibb.co.uk/m4gpie/life',
    license = "AGPL-3.0-or-later",
    keywords = "conway game of life",
    py_modules=find_packages(),
    packages=['life'],
    include_package_data=True,
    install_requires=[
        "Click",
        "PyGame",
        "NumPy",
    ],
    entry_points='''
        [console_scripts]
        life=life.main:main
    ''',
)
